#!/bin/bash
# Script to reload nginx and PHP on Homestead

echo "Restarting nginx..."
nginx -s reload

echo "Restarting PHP 5.6..."
systemctl restart php5.6-fpm

echo "Restarting PHP 7.0..."
systemctl restart php7.0-fpm

echo "Restarting PHP 7.1..."
systemctl restart php7.1-fpm

echo "Restarting PHP 7.2..."
systemctl restart php7.2-fpm

echo "Restarting PHP 7.3..."
systemctl restart php7.3-fpm

echo "Restarting PHP 7.4..."
systemctl restart php7.4-fpm

echo "Restarting PHP 8.1..."
systemctl restart php8.1-fpm
