#!/bin/bash
# Script to generate the German locale on Homestead

apt-get update
apt-get install language-pack-de
locale-gen de_DE
locale-gen de_DE.UTF-8
update-locale