# Homestead scripts

This repo contains some of my personal used scripts to update the configuration of a default Homestead install.

## Usage
Clone this repository in a folder and make sure the folder is synced to your Homestead by updating the `Homestead.yaml`.

The files are then available in the folder and can be run with 'sudo': `sudo ./restart_nginx`.

### Example
1. Clone the repo in `~/Vagrant`
2. Update the 'folders' section in your `Homestead.yaml`:
```yaml
folders:
    - map: ~/Vagrant
      to: /home/vagrant/scripts
```
3. Start Vagrant with the new config: `vagrant up --provision`
4. SSH in to your Vagrant Homestead box
5. Go to the folder: `cd /home/vagrant/scripts`
6. Execute the script: `sudo ./restart_nginx`

**_**The files must be executable before they can be run with the command. You can do this with `chmod +x filename`.**_**

## Scripts
|Script|Description|
|-|-|
|`locale_de.sh`|Installs the German locale in the system|
|`locale_nl.sh`|Installs the Dutch locale in the system|
|`php.sh`|Installs the following PHP module for the PHP versions 7.0, 7.1, 7.2, 7.3 & 7.4: sybase. Also changes the configuration of ImageMagick|
|`restart_nginx.sh`|Restarts nginx and processes for PHP versions 5.6, 7.0, 7.1, 7.2, 7.3 & 7.4|