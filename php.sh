#!/bin/bash
# Script to install some PHP modules not on Homestead by default

#apt-get update
#apt-get install php7.0-sybase php7.1-sybase php7.2-sybase php7.3-sybase php7.4-sybase php7.0-imagick php7.1-imagick php7.2-imagick php7.3-imagick php7.4-imagick

sed -i -e 's/rights="none" pattern="PDF"/rights="read|write" pattern="PDF"/' /etc/ImageMagick-6/policy.xml
