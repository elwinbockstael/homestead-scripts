#!/bin/bash
# Script to generate the Dutch locale on Homestead

apt-get update
apt-get install language-pack-nl
locale-gen nl_NL
locale-gen nl_NL.UTF-8
update-locale